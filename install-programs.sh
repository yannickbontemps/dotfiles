#!/bin/bash

echo "Installing Homebrew"
ruby -e "$(curl -fsSL https://raw.github.com/Homebrew/homebrew/go/install)"

echo "Installing Capifony"
gem install capifony

echo "Installing brew packages"
brew install bash
brew install bash-completion
brew install reattach-to-user-namespace --wrap-launchctl --wrap-pbcopy-and-pbpaste
brew install ec2-api-tools
brew install git
brew install htop
brew install tmux
brew install vim
brew install wget

echo "Installing brew cask"

brew tap phinze/cask
brew install brew-cask
brew tap caskroom/versions

echo "Installing programs"

brew cask install adium
brew cask install alfred
brew cask install appcleaner
brew cask install daisydisk
brew cask install dropbox
brew cask install dropbox-encore
brew cask install filezilla
brew cask install google-chrome
brew cask install google-hangouts
brew cask install imageoptim
brew cask install iterm2
brew cask install jumpcut
brew cask install seashore
brew cask install sequel-pro
brew cask install sketchup
brew cask install skype
brew cask install slate
brew cask install spotify
brew cask install sublime-text3
brew cask install taskpaper
brew cask install transmission
brew cask install vagrant
brew cask install vmware-fusion
brew cask install vlc
brew cask install subtitles

echo "Linking Alfred into the party"
brew cask alfred link

echo "Setting up aws cli commands"
sudo easy_install pip
sudo pip install awscli
